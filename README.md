# React frontend with laravel endpoint

Front end React SPA that fetches data from laravel endpoint

### Prerequisites

Updated version of Docker

## Start applications

Clone the repository and cd into the project folder

```bash
$> docker-compose up -d
```

## Project structure

    .
    ├── front      # All frontend project related files (Checkout folder readme for further details)
    └── back       # All backend project related files
