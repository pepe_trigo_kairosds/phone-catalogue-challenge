# laravel-react-docker

## Set up back development

```bash
$> docker-compose up -d
$> docker-compose exec machine bash
```

## Run development serve

```bash
$> php artisan serve --host=0.0.0.0 --port=8888
```

