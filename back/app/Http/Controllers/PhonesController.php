<?php

namespace App\Http\Controllers;

class PhonesController extends Controller
{
    protected function list()
    {
        $content = json_decode(file_get_contents('/app/resources/data/phones.json'), true);
        return response()->json($content);
    }
}
