import { loadPhonesSuccess, loadPhones } from "./phonesActions";
import { BEGIN_API_CALL, LOAD_PHONES_SUCCESS } from "./actions";
import phones from "../../../test/phonesMock";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe("Async Actions", () => {
    afterEach(() => {
        fetchMock.restore();
    });

    describe("Load Courses Thunk", () => {
        it("should create BEGIN_API_CALL and LOAD_HONES_SUCCESS when loading phones", () => {
        fetchMock.mock("*", {
            body: phones,
            headers: { "content-type": "application/json" }
        });

        const expectedActions = [
            { type: BEGIN_API_CALL },
            { type: LOAD_PHONES_SUCCESS, phones }
        ];

        const store = mockStore({ phones: [] });
        return store.dispatch(loadPhones()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
        });
    });
});
 
describe("Retrieve Phones from endpoint", () => {
    it("Should trigger a LOAD_PHONES_SUCCESS action", ()=> {
        const expectedAction = {
            type: LOAD_PHONES_SUCCESS,
            phones
        };

        const action = loadPhonesSuccess(phones);

        expect(action).toEqual(expectedAction);
    })
})
