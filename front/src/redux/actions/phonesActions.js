import * as types from "./actions";
import * as phonesApi from "../../api/phonesApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function loadPhonesSuccess(phones) {
  return { type: types.LOAD_PHONES_SUCCESS, phones };
}

export function loadPhones() {
  return function(dispatch) {
    dispatch(beginApiCall());
    return phonesApi
      .getPhones()
      .then(phones => {
        dispatch(loadPhonesSuccess(phones));
      })
      .catch(error => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}