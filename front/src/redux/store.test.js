import { createStore } from "redux";
import rootReducer from "./reducers/reducers";
import initialState from "./reducers/initialState";
import { loadPhonesSuccess  } from "./actions/phonesActions";
import phones from "../../test/phonesMock";

it("Should create phones store", function() {
  const store = createStore(rootReducer, initialState);

  const action = loadPhonesSuccess(phones);
  store.dispatch(action);

  const retrievedPhones = store.getState().phones;
  expect(retrievedPhones).toEqual(phones);
});