import { loadPhonesSuccess } from "../actions/phonesActions";
import phonesReducer from "./phonesReducer";
import initialState from "./initialState";
import phones from "../../../test/phonesMock";

it("should return phones data at LOAD_PHONES_SUCCESS", () => {
    const action = loadPhonesSuccess(phones);

    const newState = phonesReducer(initialState, action);

    expect(newState).toEqual(phones);
});