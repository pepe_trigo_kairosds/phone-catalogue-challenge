import { combineReducers } from "redux";
import phones from "./phonesReducer";
import apiCallsInProgress from "./apiStatusReducer";

const rootReducer = combineReducers({
  phones,
  apiCallsInProgress
});

export default rootReducer;