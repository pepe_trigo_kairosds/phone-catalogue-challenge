import * as types from "../actions/actions";
import initialState from "./initialState";

export default function phonesReducer(state = initialState.phones, action) {
  switch (action.type) {
    case types.LOAD_PHONES_SUCCESS:
      return action.phones;
    default:
      return state;
  }
}