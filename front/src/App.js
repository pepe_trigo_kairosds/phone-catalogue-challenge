import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as phoneActions from "./redux/actions/phonesActions";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Spinner from "./components/Spinner";
import PhoneList from './components/PhoneList';
import PhoneDetail from './components/PhoneDetail';
import './App.css';

class App extends React.Component {
    componentDidMount() {
        const { actions } = this.props;

        actions.loadPhones().catch(error => {
            alert("Loading phones failed" + error);
        });
    }

    render() {
        return (
            <div className="App">
                {this.props.isFetching ? (
                    <Spinner/>
                ) : (
                    <Router>
                        <Switch>
                            <Route exact path="/">
                                <PhoneList phones={this.props.phones}/>
                            </Route>
                            <Route exact path="/phone/:id" render={({match}) => (
                                <PhoneDetail phone={this.props.phones.find(phone => phone.id === parseInt(match.params.id))}/>
                            )}/>
                        </Switch>
                    </Router>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    phones: state.phones,
    isFetching: state.apiCallsInProgress
});

const mapDispatchToProps = dispatch => ({
    actions: {
        loadPhones: bindActionCreators(phoneActions.loadPhones, dispatch),
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
