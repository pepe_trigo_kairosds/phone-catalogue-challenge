import { handleResponse, handleError } from "./apiUtils";
const baseUrl = "http://localhost:8096/api/phones";

export function getPhones() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}