import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { PhoneList } from "./PhoneList";
import renderer from "react-test-renderer";
import phones from "../../test/phonesMock";

it("Displays phones listing", () => {
    const tree = renderer.create(
        <Router>
            <PhoneList
                phones = {phones}
            />
        </Router>
    );

    expect(tree).toMatchSnapshot();
})