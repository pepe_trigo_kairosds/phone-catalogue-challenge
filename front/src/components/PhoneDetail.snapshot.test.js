import React from "react";
import renderer from "react-test-renderer";
import PhoneDetail from "./PhoneDetail";
import phones from "../../test/phonesMock";

const phone = phones[0];

it("Displays phone detail", () => {
    const tree = renderer.create(
        <PhoneDetail
            phone = {phone}
        />
    );

    expect(tree).toMatchSnapshot();
})