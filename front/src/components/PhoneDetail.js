import React from "react";
import { useParams } from "react-router-dom";
import "./PhoneDetail.css";

export function PhoneDetail({phone}) {
    return (
        <article className="detail">
            <h2 className="detail__title">Details for {phone.name}</h2>
            <div className="media-object">
                <img className="detail__image" src={"https://raw.githubusercontent.com/guidesmiths/interview-code-challenges/master/react/phone-catalogue/images/" + phone.imageFileName} />
                <dl className="detail__specs">
                    <dt>Brand</dt>
                    <dd>{phone.manufacturer}</dd>
                    <dt>Color</dt>
                    <dd>{phone.color}</dd>
                    <dt>Screen</dt>
                    <dd>{phone.screen}</dd>
                    <dt>Processor</dt><dd>{phone.processor}</dd>
                    <dt>RAM</dt>
                    <dd>{phone.ram}</dd>
                </dl>
            </div>
            <p className="detail__description">{phone.description}</p>
            <p className="detail__price">{phone.price} €</p>
        </article>
    )
}

export default PhoneDetail;