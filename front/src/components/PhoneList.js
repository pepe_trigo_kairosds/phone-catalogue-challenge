import React from 'react';
import { Link } from "react-router-dom";
import './PhoneList.css';

export function PhoneList({phones}) {
    return (
        <section className="list">
            <h2 className="list__title">Phone listing</h2>
            <ul className="list__items">
                {phones.map(phone => {
                    return (<Link to={"/phone/" + phone.id} key={phone.id} className="list__item">
                        <li>{phone.name}</li>
                    </Link>)
                })}
            </ul>
        </section>
    )
}

export default PhoneList;
