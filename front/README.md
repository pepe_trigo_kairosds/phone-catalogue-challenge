# frontend react phone-catalogue

React Code Challenge

### Prerequisites

Updated version of NodeJS

### Installing

Clone the repo and cd into the directory

```
npm install
```

Once dependecies have been installed

```
npm run dev
```

Run tests

```
npm run test
```